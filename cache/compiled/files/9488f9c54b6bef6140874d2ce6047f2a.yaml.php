<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/app/user/config/site.yaml',
    'modified' => 1506399148,
    'data' => [
        'title' => 'Systemate',
        'author' => [
            'name' => 'DW'
        ],
        'metadata' => [
            'description' => 'A unique and user friendly total management system.'
        ],
        'header' => [
            'title' => 'Systemate',
            'logo' => 'sm8-logo.png',
            'description' => 'A unique and user friendly total management system.'
        ],
        'footer' => [
            'links' => [
                0 => [
                    'text' => 'Terms &amp; Conditions',
                    'url' => '#'
                ],
                1 => [
                    'text' => 'Legals',
                    'url' => '#'
                ]
            ]
        ]
    ]
];
