<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://halcyon/halcyon.yaml',
    'modified' => 1502831378,
    'data' => [
        'enabled' => true,
        'color' => 'blue',
        'dropdown' => [
            'enabled' => false
        ]
    ]
];
