<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/app/system/blueprints/config/streams.yaml',
    'modified' => 1502831376,
    'data' => [
        'title' => 'PLUGIN_ADMIN.FILE_STREAMS',
        'form' => [
            'validation' => 'loose',
            'hidden' => true,
            'fields' => [
                'schemes.xxx' => [
                    'type' => 'array'
                ]
            ]
        ]
    ]
];
