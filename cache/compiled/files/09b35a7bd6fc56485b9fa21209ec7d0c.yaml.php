<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/app/user/plugins/error/error.yaml',
    'modified' => 1502831376,
    'data' => [
        'enabled' => true,
        'routes' => [
            404 => '/error'
        ]
    ]
];
