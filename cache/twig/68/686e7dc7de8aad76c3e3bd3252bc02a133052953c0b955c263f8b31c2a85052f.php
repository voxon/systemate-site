<?php

/* modular/intro.html.twig */
class __TwigTemplate_498982fef9d742668baeb9c3dda3043268a3f7339750f2c099d31bfeeae91f23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"intro text-center section-padding\" id=\"introduction\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-md-8 col-md-offset-2 wp1\">
        ";
        // line 5
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
        echo "
        ";
        // line 6
        if ($this->getAttribute((isset($context["header"]) ? $context["header"] : null), "link", array())) {
            // line 7
            echo "      </br>

      <a href=\"";
            // line 9
            echo $this->getAttribute((isset($context["header"]) ? $context["header"] : null), "link", array());
            echo "\" class=\"btn btn-default\">";
            echo $this->getAttribute((isset($context["header"]) ? $context["header"] : null), "button", array());
            echo "</a>
    ";
        }
        // line 11
        echo "      </div>
    </div>
  </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/intro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 11,  35 => 9,  31 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"intro text-center section-padding\" id=\"introduction\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-md-8 col-md-offset-2 wp1\">
        {{ page.content }}
        {% if header.link %}
      </br>

      <a href=\"{{ header.link }}\" class=\"btn btn-default\">{{ header.button }}</a>
    {% endif %}
      </div>
    </div>
  </div>
</section>
", "modular/intro.html.twig", "/app/user/themes/halcyon/templates/modular/intro.html.twig");
    }
}
