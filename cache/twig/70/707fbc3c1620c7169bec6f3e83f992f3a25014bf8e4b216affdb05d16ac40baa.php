<?php

/* partials/header.html.twig */
class __TwigTemplate_14929bd38cff585303e6545ff0f5b3f59f3b1abb8348d1b2c6208c04aee585f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header id=\"home\" class=\"";
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "slug", array());
        echo "\">
  ";
        // line 7
        echo "  <section class=\"header\" >
    <div class=\"container\">
      <!-- <div class=\"row\">

      </div> -->
      <div class=\"row \">
        <!-- <div class=\"navicon\">
          <a id=\"nav-toggle\" class=\"nav_slide_button\" href=\"#\"><span></span></a>
        </div> -->
        <div class=\"col-md-8 col-md-offset-2 text-center \">
          <!-- <img class=\"sm8-logo-small\" src=\"";
        // line 17
        echo (isset($context["theme_url"]) ? $context["theme_url"] : null);
        echo "/img/";
        echo $this->getAttribute($this->getAttribute((isset($context["site"]) ? $context["site"] : null), "header", array()), "logo", array());
        echo "\"/> -->
          <a href=\"/\"  ><h1 class=\"\">";
        // line 18
        echo $this->getAttribute($this->getAttribute((isset($context["site"]) ? $context["site"] : null), "header", array()), "title", array());
        echo "</h1></a>

";
        // line 20
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->dump($this->env, $context, (isset($context["site"]) ? $context["site"] : null));
        echo "
        </div>
      </div>

    </div>
  </section>
</header>
";
    }

    public function getTemplateName()
    {
        return "partials/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 20,  42 => 18,  36 => 17,  24 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header id=\"home\" class=\"{{ page.slug }}\">
  {#
  {%  block navigation %}
  {% include 'partials/navigation.html.twig' %}
  {% endblock %}
  #}
  <section class=\"header\" >
    <div class=\"container\">
      <!-- <div class=\"row\">

      </div> -->
      <div class=\"row \">
        <!-- <div class=\"navicon\">
          <a id=\"nav-toggle\" class=\"nav_slide_button\" href=\"#\"><span></span></a>
        </div> -->
        <div class=\"col-md-8 col-md-offset-2 text-center \">
          <!-- <img class=\"sm8-logo-small\" src=\"{{ theme_url }}/img/{{ site.header.logo }}\"/> -->
          <a href=\"/\"  ><h1 class=\"\">{{ site.header.title }}</h1></a>

{{ dump(site) }}
        </div>
      </div>

    </div>
  </section>
</header>
", "partials/header.html.twig", "/app/user/themes/halcyon/templates/partials/header.html.twig");
    }
}
