<?php

/* partials/hero.html.twig */
class __TwigTemplate_ffe38b256ca507100f7a5f89623cdcd15ab0576b8f21ec8de622e429572b2fba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'navigation' => array($this, 'block_navigation'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header id=\"home\" class=\"";
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "slug", array());
        echo "\">
  ";
        // line 2
        $this->displayBlock('navigation', $context, $blocks);
        // line 5
        echo "
  <div class=\"dw-navicon\">
    <a id=\"nav-toggle\" class=\"nav_slide_button\" href=\"#\"><span></span></a>
  </div>

  <section class=\"hero align_middle_parent\" id=\"hero\">



    <div class=\"container align_middle\">

        <div class=\" text-center \">
          <img class=\"animated fadeInDown sm8-logo\" src=\"";
        // line 17
        echo (isset($context["theme_url"]) ? $context["theme_url"] : null);
        echo "/img/";
        echo $this->getAttribute($this->getAttribute((isset($context["site"]) ? $context["site"] : null), "header", array()), "logo", array());
        echo "\"/>
          <h1 class=\"hero_title animated fadeInDown\">";
        // line 18
        echo $this->getAttribute($this->getAttribute((isset($context["site"]) ? $context["site"] : null), "header", array()), "title", array());
        echo "</h1>
          <p class=\"animated fadeInUp delay-05s\">";
        // line 19
        echo $this->getAttribute($this->getAttribute((isset($context["site"]) ? $context["site"] : null), "header", array()), "description", array());
        echo "</p>
        </div>

    </div>
  </section>
</header>
";
    }

    // line 2
    public function block_navigation($context, array $blocks = array())
    {
        // line 3
        echo "  ";
        $this->loadTemplate("partials/navigation.html.twig", "partials/hero.html.twig", 3)->display($context);
        // line 4
        echo "  ";
    }

    public function getTemplateName()
    {
        return "partials/hero.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 4,  65 => 3,  62 => 2,  51 => 19,  47 => 18,  41 => 17,  27 => 5,  25 => 2,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header id=\"home\" class=\"{{ page.slug }}\">
  {% block navigation %}
  {% include 'partials/navigation.html.twig' %}
  {% endblock %}

  <div class=\"dw-navicon\">
    <a id=\"nav-toggle\" class=\"nav_slide_button\" href=\"#\"><span></span></a>
  </div>

  <section class=\"hero align_middle_parent\" id=\"hero\">



    <div class=\"container align_middle\">

        <div class=\" text-center \">
          <img class=\"animated fadeInDown sm8-logo\" src=\"{{ theme_url }}/img/{{ site.header.logo }}\"/>
          <h1 class=\"hero_title animated fadeInDown\">{{ site.header.title }}</h1>
          <p class=\"animated fadeInUp delay-05s\">{{ site.header.description }}</p>
        </div>

    </div>
  </section>
</header>
", "partials/hero.html.twig", "/app/user/themes/halcyon/templates/partials/hero.html.twig");
    }
}
