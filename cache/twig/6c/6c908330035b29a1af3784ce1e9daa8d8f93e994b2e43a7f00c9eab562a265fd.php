<?php

/* modular/portfolio.html.twig */
class __TwigTemplate_a53400cd62c7d7bcfb136d2f629944c70a68a2bbc3652a89fff7db6b8a42e587 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"portfolio text-center section-padding\" id=\"portfolio\">
  <div class=\"container\">
    <div class=\"row\">
      ";
        // line 4
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
        echo "
      <div id=\"portfolioSlider\">
        <ul class=\"slides\">
          ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "portfolio", array()), 2));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 8
            echo "          <li>
            ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["row"]);
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 10
                echo "            <div class=\"col-md-6 wp4 ";
                if ($this->getAttribute($context["item"], "delay", array())) {
                    echo "delay-";
                    echo $this->getAttribute($context["item"], "delay", array());
                    echo "s";
                }
                echo "\">
              <div class=\"overlay-effect effects clearfix\">
                <div class=\"img \" >

                    <video class=\"feature_video\" loop preload=\"none\" poster=\"";
                // line 14
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["item"], "img", array()), array(), "array"), "url", array());
                echo "\">
                      <source src=\"";
                // line 15
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["item"], "mp4", array()), array(), "array"), "url", array());
                echo "\" type=\"video/mp4\">
                      <source src=\"";
                // line 16
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["item"], "webm", array()), array(), "array"), "url", array());
                echo "\" type=\"video/webm\">
                      <
                    </video>

                  <div class=\"overlay \">
                     <i class=\"fa fa-play vcenter\"></i>



                  </div>
                </div>
              </div>





              <h2>";
                // line 33
                echo $this->getAttribute($context["item"], "title", array());
                echo "</h2>
              <p>";
                // line 34
                echo $this->getAttribute($context["item"], "content", array());
                echo "</p>

            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "          </li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "        </ul>
      </div>
    </div>
  </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/portfolio.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 40,  95 => 38,  85 => 34,  81 => 33,  61 => 16,  57 => 15,  53 => 14,  41 => 10,  37 => 9,  34 => 8,  30 => 7,  24 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"portfolio text-center section-padding\" id=\"portfolio\">
  <div class=\"container\">
    <div class=\"row\">
      {{ page.content }}
      <div id=\"portfolioSlider\">
        <ul class=\"slides\">
          {% for row in page.header.portfolio|batch(2) %}
          <li>
            {% for item in row %}
            <div class=\"col-md-6 wp4 {% if item.delay %}delay-{{ item.delay }}s{% endif %}\">
              <div class=\"overlay-effect effects clearfix\">
                <div class=\"img \" >

                    <video class=\"feature_video\" loop preload=\"none\" poster=\"{{ page.media[item.img].url }}\">
                      <source src=\"{{ page.media[item.mp4].url }}\" type=\"video/mp4\">
                      <source src=\"{{ page.media[item.webm].url }}\" type=\"video/webm\">
                      <
                    </video>

                  <div class=\"overlay \">
                     <i class=\"fa fa-play vcenter\"></i>



                  </div>
                </div>
              </div>





              <h2>{{ item.title }}</h2>
              <p>{{ item.content }}</p>

            </div>
            {% endfor %}
          </li>
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>
", "modular/portfolio.html.twig", "/app/user/themes/halcyon/templates/modular/portfolio.html.twig");
    }
}
