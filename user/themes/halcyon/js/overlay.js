$(document).ready(function() {
  alert('ready ');

  if (Modernizr.touch) {
    // show the close overlay button
    // $(".close-overlay").removeClass("hidden");
    // handle the adding of hover class when clicked
    $(".effects .img").click(function(e) {
      e.preventDefault();
      e.stopPropagation();
      if (!$(this).hasClass("hover")) {
        $(this).addClass("hover");

        $(this).find('.feature_video').each(this.play());
          console.log('play video');
      }

    });
    // handle the closing of the overlay
    // $(".close-overlay").click(function(e) {
    //   e.preventDefault();
    //   e.stopPropagation();
    //   if ($(this).closest(".img").hasClass("hover")) {
    //     $(this).closest(".img").removeClass("hover");
    //   }
    //
    // });
  } else {
        console.log('no Modernizr');
    // handle the mouseenter functionality
    $(".effects .img").mouseenter(function() {
      $(this).addClass("hover");
      $(this).find('video').each(this.play());
        console.log('play video');
    })
    // handle the mouseleave functionality
    .mouseleave(function() {
      $(this).removeClass("hover");
      $(this).find('video').each(this.pause());
        console.log('pause video');
    });
  }
});
