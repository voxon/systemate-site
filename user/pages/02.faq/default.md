---
title: Frequently Asked Questions
menu: FAQ
heading: Systemate
slug: frequently-asked-questions
---
##What is Systemate?

Systemate is a flexible cloud and mobile based platform designed to build tailored business applications quickly and easily without requiring programming.

Systemate provides an easy to use interface with features to design and manage: databases, forms, training, compliance, events, tasks, process automation and real-time reporting. Systemate is available in the cloud, on premises and offline via mobile devices.

##How does Systemate work?

Once a Systemate account is setup it is accessed via the internet or local network depending on the client’s installation (in the cloud or on premises). Systemate also includes a mobile app that can be installed on any iOS or Android device which connects to the cloud or on premises installation. The mobile app also has the ability to work offline in remote areas without internet connection. Users get access to different areas of systems depending on their privileges.

Generally, users will interact with tailored forms that pull data from custom databases. When submitted these forms trigger automation to process the data or perform other tasks. While these processes are highly configurable, general examples include inserting/updating data in databases, creating events, creating tasks, saving records of compliance, delivering training or updating financial ledgers.

Records of all interactions are automatically saved as audits insuring transparent and accountable record keeping.

##What are the advantages of using Systemate?

Systemate streamlines processes removing the need to double triple handle data, eliminating the chance of data corruption or manipulation. Systemate collects data at the source immediately making it available for processing and reporting.

Systemate automatically saves audits of all interactions providing clear and transparent records and accountability.

Systemate is easy to use and configure with drag and drop tool to build forms, databases and workflows.

Systemate can be continually refined and configured to improve data collection, processing and workflows at anytime without the need for programmers or effecting existing data. This allows Systemate to adapt immediately to changing information management requirements.
