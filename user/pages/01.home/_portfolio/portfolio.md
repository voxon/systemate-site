---
title: Features
portfolio:
  - title: "Easily Customised"
    img: easily_customised.jpg
    mp4: easily_customised.mp4
    webm: easily_customised.webm
    content: "Quickly customise databases and forms to capture relevant data."
  - title: "Fast Deployment"
    img: Fast_Deployment.jpg
    mp4: Fast_Deployment.mp4
    webm: Fast_Deployment.webm
    content: "Changes are instantly available across all devices."
    delay: 0.5
  - title: "Offline Mode"
    img: portfolio-03.jpg
    media: Easily_Customised
    content: "No internet? No problem! Cache targeted data offline for remote lookups and processing."

  - title: "Realtime Reporting"
    img: Reporting.jpg
    mp4: Reporting.mp4
    webm: Reporting.webm
    content: "Visualise data on any metric in real time."
  - title: "Ensure Transparency"
    img: Audits.jpg
    mp4: Audits.mp4
    webm: Audits.webm
    media: Easily_Customised
    content: "Comprehensive auditing ensure transparency and peace of mind."
  - title: "Automation"
    img: portfolio-03.jpg
    media: Easily_Customised
    content: "Automate complex workflows."

---

<!-- # Core Features {.arrow} -->
