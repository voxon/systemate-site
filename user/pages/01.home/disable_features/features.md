---
title: Features
features:
  - title: "Easy to use and customise"
    icon: flask shadow
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
    link: ""
    text: ""
  - title: "Fast deployment across devices"
    icon: tablet shadow
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
    link: ""
    text: ""
    delay: "05"
  - title: "Offline Mode"
    icon: database shadow
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
    link: ""
    text: ""
    delay: "1"

  - title: "Easy to use and customise"
    icon: flask shadow
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
    link: ""
    text: ""
  - title: "Fast deployment across devices"
    icon: tablet shadow
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
    link: ""
    text: ""
    delay: "05"
  - title: "Offline Mode"
    icon: database shadow
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
    link: ""
    text: ""
    delay: "1"

---

#   Core Features {.arrow}
